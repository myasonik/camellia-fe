import { includesErrorTypes } from 'camellia-fe/gql/errors/utils';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { inject as service } from '@ember/service';
import ApolloService from 'ember-apollo-client/services/apollo';

export default class OverriddenApollo extends ApolloService {
  @service session;

  link() {
    let httpLink = super.link();

    let authLink = setContext((request, context) => {
      return this._authorize(request, context);
    });

    const resetToken = onError((errors) => {
      if (this._hasAuthError(errors)) {
        this.session.invalidate('authenticator:custom');
        return;
      }
    });

    return authLink.concat(resetToken).concat(httpLink);
  }

  _authorize() {
    if (!this.session.isAuthenticated) {
      return {};
    }

    return new Promise((success) => {
      let { authenticated } = this.session.data;
      let { credentials } = authenticated?.userLogin ?? authenticated?.userRegister;
      let { accessToken, uid, tokenType, client, expiry } = credentials;

      success({
        headers: {
          'access-token': accessToken,
          'token-type': tokenType,
          uid,
          client,
          expiry,
        },
      });
    });
  }

  _hasAuthError({ graphQLErrors, networkError }) {
    if (networkError?.statusCode === 401) return true;
    if (!graphQLErrors?.length) return false;

    return includesErrorTypes(graphQLErrors, ['AUTHENTICATION_ERROR', 'USER_ERROR']);
  }
}
