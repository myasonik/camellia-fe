import signOut from 'camellia-fe/gql/mutations/sign-out.graphql';
import { queryManager } from 'ember-apollo-client';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';

export default class NavComponent extends Component {
  @service session;
  @queryManager apollo;

  @action
  async signOut(e) {
    e.preventDefault();

    await this.apollo.mutate({ mutation: signOut });

    this.session.invalidate('authenticator:custom');
  }
}
