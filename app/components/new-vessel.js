import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import createVessel from 'camellia-fe/gql/mutations/create-vessel.graphql';
import { queryManager } from 'ember-apollo-client';
import { validatePresence } from 'ember-changeset-validations/validators';
import { dropTask } from 'ember-concurrency-decorators';
export default class NewVesselComponent extends Component {
  @queryManager apollo;
  @service router;

  vessel = {
    name: '',
    vesselType: false, // is niche?
  };

  validations = {
    name: [validatePresence({ presence: true, ignoreBlank: true })],
  };

  @action
  handleSubmit(changeset, event) {
    event.preventDefault();
    changeset.validate();

    if (changeset.isValid) this.submit.perform(changeset);
  }

  @dropTask
  submit = function* (changeset) {
    try {
      let {
        createVessel: { message },
      } = yield this.apollo.mutate({
        mutation: createVessel,
        variables: {
          name: changeset.get('name'),
          vesselType: changeset.get('vesselType') ? 'NICHE' : 'STANDARD',
        },
      });

      if (message === 'Validation failed: Name has already been taken') {
        throw new Error('DUPLICATE_NAME');
      }

      yield this.router.transitionTo('authenticated.home');
    } catch (error) {
      if (error.message === 'DUPLICATE_NAME') {
        changeset.addError('name', ['Name has already been taken']);
      } else {
        // TODO
        console.log(error);
      }
    }
  };
}
