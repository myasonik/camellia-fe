import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export default class ValidatedFieldComponent extends Component {
  inputId = `${guidFor(this)}-input`;

  @tracked
  changesetProperty = this.args.changeset.get(this.args.property);

  @action
  input({ target }) {
    const value = this.args.type === 'checkbox' ? target.checked : target.value;
    this.args.changeset.set(this.args.property, value);
  }

  @action
  blur() {
    this.args.changeset.validate(this.args.property);
  }
}
