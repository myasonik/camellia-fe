import { queryManager } from 'ember-apollo-client';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import signUp from 'camellia-fe/gql/mutations/sign-up.graphql';

export default class SignInComponent extends Component {
  @tracked errorMessage;
  @queryManager apollo;
  @service session;

  _submitSignUp() {
    let { email, password } = this;

    return this.apollo.mutate({ mutation: signUp, variables: { email, password } });
  };

  @action
  updateEmail(e) {
    this.email = e.target.value;
  }

  @action
  updatePassword(e) {
    this.password = e.target.value;
  }

  @action
  async authenticate(e) {
    e.preventDefault();

    try {
      await this.session.authenticate('authenticator:custom', this._submitSignUp);
    } catch (error) {
      this.errorMessage = error.message ?? error.errors[0].message;
      return;
    }
  }
}
