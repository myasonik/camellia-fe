import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import editVessel from 'camellia-fe/gql/mutations/edit-vessel.graphql';
import { queryManager } from 'ember-apollo-client';
import { dropTask } from 'ember-concurrency-decorators';

export default class NewVesselComponent extends Component {
  @queryManager apollo;
  @service router;

  nameId = `${guidFor(this)}-name`;
  nameIdError = `${guidFor(this)}-name`;
  nicheId = `${guidFor(this)}-niche`;

  @tracked vesselName = this.args.vessel.name;
  @tracked isNiche = this.args.vessel.vesselType === 'NICHE';
  @tracked missingName = false;
  @tracked duplicateName = false;

  get hasError() {
    if (this.missingName || this.duplicateName) return this.nameIdError;
    return '';
  }

  focusName() {
    document.getElementById(this.nameId).focus();
  }

  @action
  inputChange() {
    this.missingName = false;
    this.duplicateName = false;
  }

  @dropTask
  handleSubmit = function* (event) {
    event.preventDefault();

    if (this.vesselName.length === 0) {
      this.missingName = true;
      this.focusName();
      return;
    }

    try {
      let {
        editVessel: { message },
      } = yield this.apollo.mutate({
        mutation: editVessel,
        variables: {
          vesselId: this.args.vessel.id,
          name: this.vesselName,
          vesselType: this.isNiche ? 'NICHE' : 'STANDARD',
        },
      });

      if (message === 'Validation failed: Name has already been taken') {
        throw new Error('DUPLICATE_NAME');
      }

      yield this.router.transitionTo('authenticated.home');
    } catch (error) {
      if (error.message === 'DUPLICATE_NAME') {
        this.duplicateName = true;
        this.focusName();
      } else {
        // TODO
        console.log(error);
      }
    }
  };
}
