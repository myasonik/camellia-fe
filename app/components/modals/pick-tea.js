import Component from '@glimmer/component';
import steeping from 'camellia-fe/gql/fragments/steeping.graphql';
import createServingWithSteeping from 'camellia-fe/gql/mutations/create-serving-with-steeping.graphql';
import { queryManager } from 'ember-apollo-client';
import { dropTask } from 'ember-concurrency-decorators';

export default class ModalsPickTeaComponent extends Component {
  @queryManager apollo;

  @dropTask
  createServing = function* (teaId) {
    let { activeVessel } = this.args;

    try {
      yield this.apollo.mutate({
        mutation: createServingWithSteeping,
        variables: {
          teaId,
          vesselId: activeVessel.id,
        },
        update(cache, { data }) {
          cache.modify({
            id: cache.identify(activeVessel),
            fields: {
              serving() {
                const newServingRef = cache.writeFragment({
                  data: data.createServingWithSteeping.serving,
                  fragment: steeping,
                });
                return newServingRef;
              },
            },
          });
        },
      });
    } catch (e) {
      // TODO
      console.log(e);
    } finally {
      this.args.close();
    }
  };
}
