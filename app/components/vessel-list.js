import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import steeping from 'camellia-fe/gql/fragments/steeping.graphql';
import decommissionVessel from 'camellia-fe/gql/mutations/decommission-vessel.graphql';
import resteep from 'camellia-fe/gql/mutations/resteep.graphql';
import trashServing from 'camellia-fe/gql/mutations/trash-serving.graphql';
import { queryManager } from 'ember-apollo-client';
import { dropTask } from 'ember-concurrency-decorators';

export default class VesselListComponent extends Component {
  @queryManager apollo;

  @tracked activeVessel = null;
  @tracked showPickTeaFlow = false;
  @tracked showEditVesselFlow = false;

  @action
  closeModals() {
    this.activeVessel = null;
    this.showPickTeaFlow = false;
  }

  @action
  pickTea(vessel) {
    this.activeVessel = vessel;
    this.showPickTeaFlow = true;
  }

  @action
  editVessel(vessel) {
    this.activeVessel = vessel;
    this.showEditVesselFlow = true;
  }

  @dropTask
  resteep = function* (vessel) {
    try {
      yield this.apollo.mutate({
        mutation: resteep,
        variables: {
          servingId: vessel.serving.id,
        },
        update(cache, { data }) {
          cache.modify({
            id: cache.identify(vessel.serving),
            fields: {
              steepings(steepings = []) {
                const newSteepingRef = cache.writeFragment({
                  data: data.createSteeping.steeping,
                  fragment: steeping,
                });

                return [...steepings, newSteepingRef];
              },
            },
          });
        },
      });
    } catch (e) {
      // TODO
      console.log(e);
    }
  };

  @dropTask
  trashServing = function* (vessel) {
    try {
      yield this.apollo.mutate({
        mutation: trashServing,
        variables: {
          servingId: vessel.serving.id,
        },
        update(cache) {
          cache.modify({
            id: cache.identify(vessel),
            fields: {
              serving(_, { DELETE }) {
                return DELETE;
              },
            },
          });
        },
      });
    } catch (e) {
      // TODO
      console.log(e);
    }
  };

  @dropTask
  decommissionVessel = function* (vessel) {
    try {
      yield this.apollo.mutate({
        mutation: decommissionVessel,
        variables: {
          vesselId: vessel.id,
        },
        update(cache) {
          cache.modify({
            fields: {
              vessels(vesselRefs, { readField }) {
                return vesselRefs.filter((ref) => vessel.id !== readField('id', ref));
              },
            },
          });
        },
      });
    } catch (e) {
      // TODO
      console.log(e);
    }
  };
}
