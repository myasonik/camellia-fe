import Base from 'ember-simple-auth/authenticators/base';

export default class CustomAuthenticator extends Base {
  restore(data) {
    return Promise.resolve(data);
  }

  authenticate(mutationCallback) {
    return mutationCallback();
  }
}
