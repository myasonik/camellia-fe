export function includesErrorTypes(graphQLErrors, targetTypes) {
  const errorTypes = graphQLErrors.map((error) => error.extensions?.code);

  return errorTypes.some((error) => targetTypes.includes(error));
}
