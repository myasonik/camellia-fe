import EmberRouter from '@ember/routing/router';
import config from 'camellia-fe/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('sign-in');

  this.route('authenticated', { path: '' }, function () {
    this.route('home', { path: '' });

    this.route('teas', function () {
      this.route('new');
    });

    this.route('vessels', function () {
      this.route('new');
      this.route('edit', { path: '/edit/:id' });
    });
  });

  this.route('not-found', { path: '/*path' });
});
