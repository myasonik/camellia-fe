import Route from '@ember/routing/route';
import query from 'camellia-fe/gql/queries/home.graphql';
import { queryManager } from 'ember-apollo-client';
import { inject as service } from '@ember/service';

export default class IndexRoute extends Route {
  @queryManager apollo;
  @service session;

  async model() {
    return this.apollo.watchQuery({ query, fetchPolicy: 'cache-and-network' });
  }
}
