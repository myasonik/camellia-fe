import Route from '@ember/routing/route';
import query from 'camellia-fe/gql/queries/vessel.graphql';
import { queryManager } from 'ember-apollo-client';

export default class AuthenticatedVesselsEditRoute extends Route {
  @queryManager apollo;

  async model({ id }) {
    let { vessels } = await this.apollo.watchQuery({ query, fetchPolicy: 'cache-and-network' });
    return vessels.filter((vessel) => vessel.id === id)[0];
  }
}
